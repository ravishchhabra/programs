﻿using NUnit.Framework;

namespace ProgramsTest
{
    [TestFixture]
    public class ProgramsTest
    {
        [Test]
        public void IsPalindromTest()
        {
            var input = "MADAM";
            string output = Programs.Programs.IsPalindrom(input);

            // Assert
            Assert.That("TRUE", Is.EqualTo(output));
        }

        [Test]
        public void AngleTest()
        {
            var input = "4:20";
            string output = Programs.Programs.Angle(input);

            // Assert
            Assert.That("10 degrees", Is.EqualTo(output));
        }

        [Test]
        public void IsInstaTest()
        {
            var input = "ISOGRAM";
            string output = Programs.Programs.IsInsta(input);

            // Assert
            Assert.That("HETEROGRAM", Is.EqualTo(output));
        }
    }
}
