﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programs
{
    public static class Programs
    {
        public static string IsPalindrom(string input)
        {
            if (string.IsNullOrEmpty(input))
                return "UNDETERMINED";

            int length = input.Length - 1;
            string reverse = string.Empty;
            for (int index = length; index >= 0; index--)
            {
                reverse += input[index];

            }
            return string.Equals(input, reverse, StringComparison.InvariantCultureIgnoreCase) ? "TRUE" : "FALSE";
        }

        public static string Angle(string input)
        {
            if (string.IsNullOrEmpty(input))
                input = DateTime.Now.ToLongTimeString();

            string[] time = input.Split(':');

            int hour = Convert.ToInt32(time[0]);
            int min = Convert.ToInt32(time[1]);

            double hAngle = 0.5D * (hour * 60 + min);
            double mAngle = 6 * min;
            double angle = Math.Abs(hAngle - mAngle);
            angle = Math.Min(angle, 360 - angle);
            return angle + " degrees";
        }

        public static string IsInsta(string input)
        {
            var group = from character in input.ToLower().ToCharArray()
                        where char.IsLetter(character)
                        group character by character into master
                        select new { Count = master.Count() };

            bool isHeterogram = true;
            isHeterogram = group.Any(g => g.Count != 1);
            bool isoGram = true;
            if (isHeterogram)
            {
                isoGram = group.Any(g => group.Any(n => n.Count != g.Count));
            }
            return !isHeterogram ? "HETEROGRAM" : !isoGram ? "ISOGRAM" : "NOTAGRAM";
        }
    }
}
